# Onion Clock

Displays the time and date.

#### Clone Repo

    git clone git@gitlab.com:robertmermet/onion-clock.git

#### View Demo

>**demo** [robertmermet.com/projects/onion-clock](http://robertmermet.com/projects/onion-clock)